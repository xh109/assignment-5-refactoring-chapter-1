import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

public class StatementTests {
    @Test
    public void testStatement() {
        String expected = "Statement for BigCo\n" +
                "  Hamlet: $650.00 (55 seats)\n" +
                "  As You Like It: $580.00 (35 seats)\n" +
                "  Othello: $500.00 (40 seats)\n" +
                "Amount owed is $1730.00\n" +
                "You earned 47 credits\n";

        Play p1 = new Play("hamlet", "Hamlet", "tragedy");
        Play p2 = new Play("as-like", "As You Like It", "comedy");
        Play p3 = new Play("othello", "Othello", "tragedy");
        ArrayList<Play> pList = new ArrayList<Play>();
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        Performance per1 = new Performance("hamlet", 55);
        Performance per2 = new Performance("as-like", 35);
        Performance per3 = new Performance("othello", 40);
        ArrayList<Performance> perList = new ArrayList<Performance>();
        perList.add(per1);
        perList.add(per2);
        perList.add(per3);
        String customer = "BigCo";
        BillPrint app = new BillPrint(pList, customer, perList);

        assertEquals(expected,app.statement(app,pList));
    }
}
